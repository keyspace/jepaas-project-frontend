/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-15 13:24:57
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-15 13:41:12
 */
import { LOG_GET_LISTDATA } from './api';
import fetch from '../../../util/fetch';
/*
 *获取冒泡列表
 */
export function getListData(param) {
  return fetch(LOG_GET_LISTDATA, null, {
    type: 'POST',
    data: param,
  })
    .then(data => data)
    .catch();
}
