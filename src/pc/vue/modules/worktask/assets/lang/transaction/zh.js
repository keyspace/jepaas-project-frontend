const m = {
  createtransaction: '创建事务',
  keyword: '关键字',
  search: '查询',
  delay: '已延期',
  publisher: '发布人',
  finishjob: '完成任务',
  picktask: '领取任务',
  annotation: '批注',
  boot: '启动',
  pause: '暂停',
  unstar: '取消加星',
  star: '加星',
  edit: '编辑',
  del: '删除',
};
export default m;
