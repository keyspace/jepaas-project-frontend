import {
  POST_SILIANHUAN_DATAS, NEWS_CLICK_READ, NEWS_CLICK_DOUPDATE, POST_GET_INFO_BY_ID, NEWS_NAV_DATA,
} from './api';

/**
 *获取新闻公告信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function getNews(param) {
  return JE.fetch(POST_SILIANHUAN_DATAS, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
/**
 *已读操作
 * @param param
 * @returns {Promise<T | never>}
 */
export function readFuncEdit(param) {
  return JE.fetch(NEWS_CLICK_READ, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
/**
 *阅读数操作
 * @param param
 * @returns {Promise<T | never>}
 */
export function doUpdate(param) {
  return JE.fetch(NEWS_CLICK_DOUPDATE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
/**
 *根据id查询数据
 * @param param
 * @returns {Promise<T | never>}
 */
export function getById(param) {
  return JE.fetch(POST_GET_INFO_BY_ID, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
/**
 *查询导航栏的数据数据
 * @param param
 * @returns {Promise<T | never>}
 */
export function getNav(param) {
  return JE.fetch(NEWS_NAV_DATA, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
