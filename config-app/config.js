/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2021-05-08 14:09:04
 * @LastEditors: qinyonglian
 * @LastEditTime: 2021-05-08 14:16:15
 */
const merge = require('webpack-merge');
const fs = require('fs');
const path = require('path');

// 服务配置
let config = {
  server: '0.0.0.0',
  serverPort: '3000',
};
// 入口信息
const entryInfo = {
  plugins: ['login', 'my', 'news','workflow','operationlog'],
};

// 项目配置信息
const product = (config.product = process.env.PRODUCT_CONFIG || 'suanbanyun-dev');
if (product != 'je') {
  // eslint-disable-next-line import/no-dynamic-require
  config = merge(config, require(`./pro/${product}.json`));
}

// 自定义配置
const custom = path.resolve(__dirname, './config.json');
if (fs.existsSync(custom)) {
  config = merge(config, require('./config.json'));
} else {
  config = merge(config, require('./config/config.json'));
}

config.entryInfo = entryInfo;
module.exports = config;
