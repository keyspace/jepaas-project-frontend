const m = {
  alert1: '不可以发送空内容',
  alert2: '输入内容超过60字',
  alert3: '冒泡成功',
  seletesuccessfully: '删除成功',
  contentcantempty: '内容不能为空哦',
  commentssuccessfully: '发布评论成功',
  commentsfail: '发布评论失败',
  pictureundefine: '图片不存在',
  trylater: '操作频繁，请稍后再试',
  modifiedsuccessfully: '修改成功',
  needsaveit: '内容有改动，需要保存吗',
  savesuccessfully: '保存成功',
  uploadmaxnum: '已达到最大上传个数，请删除后在进行上传',
};

export default m;
