/*
 * @Descripttion: 
 * @Author: qinyonglian
 * @Date: 2021-03-04 15:00:42
 * @LastEditors: qinyonglian
 * @LastEditTime: 2021-03-04 15:29:12
 */
import { install } from '../../install.js';
import Shkty from './index.vue';



// 加载依赖平台信息
JE.loadChartScript(true);

// 安装组件
install('Shkty', Shkty);