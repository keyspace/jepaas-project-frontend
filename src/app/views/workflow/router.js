/**
 * @Author : ZiQin Zhai
 * @Date : 2020/8/31 18:30
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2020/8/31 18:30
 * @Description
 * */
import routerPage from '@/components/template/index';
import createRouter from '@/util/RouterModel';
import startPage from './pages/workStart/index.vue';
import index from './index.vue';

export default [{
  path: '/JE-PLUGIN-WF',
  component: routerPage,
  children: [
    createRouter(
      {
        path: '',
        name: 'index',
        meta: {
        },
        component: index,
      }
    ),
    createRouter({
      path: 'do_flow',
      name: 'doFlow',
      title: '发起流程',
      meta: {
        noWork: true,
      },
      component: startPage,
    }),
  ],
}];
