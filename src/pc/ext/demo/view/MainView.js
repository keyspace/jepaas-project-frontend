/**
 * 自定义功能主面板
 */
Ext.define("PRO.demo.view.MainView", {
	extend: 'Ext.panel.Panel',
	/**
	 * 声明面板的xtype
	 * 确保全局唯一，widget.是系统必带的前缀
	 * 此类xtype的引用为：test.mainview
	 * @type String
	 */
	alias: 'widget.demo.mainview',
	/**
	 * 布局
	 * @type String
	 */
	layout:'border',
    initComponent: function(){
    	var me = this;
    	//子面板的默认属性，如果存在，不会被覆盖
    	me.defaults = {
    		mainview:me//绑定主面板，方便后续调用
    	};
    	
    	//主面板通常用来规划页面结构，不做太多的操作
    	me.items = [{
    		xtype:'demo.westview',
    		region:'west',
    		itemId:'westview',
    		split:true,//允许左右调整大小
    		width:200
    	},{
    		xtype:'demo.centerview',
    		region:'center',
    		itemId:'centerview'
    	},{
            region:"south",
            height:200,
            xtype :'jechartview',
            chartCode:"JE_DEMO_GXHTB",
            chartParams:{}
        },{
            xtype:'jeportalview',
            region:'east',
            beanId:"csVXpmKVb7qoognnu3r",
            itemId:'eastview',
            split:true,//允许左右调整大小
            width:400
        }];
    	
        //面板的配置项要在父类方法前写完，否则不起效
        me.callParent(arguments);
        
        //初始化面板后就可以获取面板了
        
        //这里可以预先绑定子面板，方便后续调用
        me.westview = me.getCmp('westview');
        me.centerview = me.getCmp('centerview');
        
    }
});