/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-15 13:24:41
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-15 13:34:37
 */
// 服务器地址
import { HOST_BASIC } from '../../../constants/config';

// 列表数据
export const LOG_GET_LISTDATA = `${HOST_BASIC}/####`;
