const spe = {
  '流程工作台': 'Workbench',
  '我的消息': 'Messages',
  '交办事项': 'Assignment',
  '待我审批': 'Pending approval',
  '已办理': 'Processed',
  '我的流程': 'Business processes',
  '已延时': 'Delayed',
  '批注': 'Notation',
  '通知': 'Notifications',
  '冒泡': 'Post',
  '待我处理': 'Todo',
  '我的安排': 'Arranging',
  '历史事务': 'Bygone',
  '未读': 'Unread',
  '已读': 'Hasread',
  '已读': 'Read',
  '全部': 'All',
  'all': 'All'
};
export default spe;