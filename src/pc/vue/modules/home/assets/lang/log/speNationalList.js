const spe = {
  '日志': 'Log ',
  '今日': 'Today ',
  '明日': 'Tomorrow ',
  '周报': 'Week ',
  '本周': 'This week ',
  '下周': 'Next week ',
  '月报': 'Month ',
  '本月': 'This month ',
  '下月': 'Next month ',
  '已完成工作': 'Finished',
  '未完成工作': 'Unfinished',
  '明日计划': "Tomorrow's plan",
  '工作心得': 'Experience',
  '附件': 'Attachment',
  '下周计划': 'Next week plan',
  '下月计划': 'Next month plan',
  '单选': 'Single',
  '多选': 'Multiple',
  '全部': 'All',
  '周': ' Week ',
  '月': ' Month ',
  '暂无内容': 'No content',
  '请输入您的回复信息': 'Please enter your reply',
  '请输入您的点评信息': 'Please enter your review information',
  '发布点评': 'Published review',
  '发布回复': 'Post replies',
  '请输入点评内容': 'Please enter comments',
  '请输入回复内容': 'Please enter your reply',
  '评论成功': 'Comment successful',
  '点评信息删除成功': 'Comment information deleted successfully',
  '请填写工作总结': 'Please fill in the summary of your work',
  '点评成功': 'Comment on success',
  '保存成功': 'Save success',
};
export default spe;